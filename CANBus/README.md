# Overview
At this point you should have a basic understanding of how the CAN bus actually works. If not make sure you do. A very quick overview is that any node on the network can transmit a message to the bus, it is then up to any node to receive this information decide if it contains data that it wants to operate on it and how it decodes the data bytes. There is no master node, just messages.

## Hardware

For the rest of the tutorial it is assumed to be using the STM32 Nucleo 64 board, with the STM32F072 chip on it. [Here](https://www.st.com/en/evaluation-tools/nucleo-f072rb.html). This does however, require the use of an external CAN transceiver, used to translate the microcontrpller signals to the bus voltages and also to protect the microcontroller from ESD ect. The hardware setup to connect the STM to the bus is very simple, connect a [TI SN65HVD230](http://www.ti.com/product/SN65HVD230). The connections are incredibly simple, noting that the labels *CANTX* and *CANRX* nets are connected to the CAN_TX and CAN_RX pins of the microcontroller respectively.

![CAN Hardware Setup](./images/can_hardware.png)

Whilst this snippet is far from complete it provides a good understanding of the requirements. There are various daughterboards available for the Nucleo board, however a simple chip and a steady soldering iron would usually suffice!

![CAN Hardware Setup](./images/nucleo_mod5.jpg)

# STM32CubeMX

The basic STM32CubeMX setup for 1MBit CAN setup is:

![CAN Cube Setup](./images/1Mbit_cube.png)

There is an example for the STMF072 and STM32F042 Cube setups.

# HAL Code

## Transmit

~~~ C
CanTxMsgTypeDef sTxMessage;
sTxMessage.DLC = 6;
sTxMessage.Data[0] = 0xC;
sTxMessage.Data[1] = 0;
sTxMessage.Data[2] = 0xF;
sTxMessage.Data[3] = 0xF;
sTxMessage.Data[4] = 0xE;
sTxMessage.Data[5] = 0xE;
sTxMessage.Data[6] = 0;
sTxMessage.Data[7] = 0;
sTxMessage.StdId = 2;
sTxMessage.IDE = CAN_ID_STD;
sTxMessage.RTR = CAN_RTR_DATA;
hcan.pTxMsg = &sTxMessage; // Noting hcan is the can bus defined by MX_CAN_Init();

HAL_CAN_Transmit_IT(&hcan);
~~~

## Recieve - ID List

### Setting up Filter List

This is global:
~~~ C

#define CANID_1 1
#define CANID_2 2
#define CANID_3 3

volatile CanTxMsgTypeDef sRxMessage
~~~

This is in main.c, but only needs to be run once
~~~ C
hcan.pRxMsg = &sRxMessage; // Noting hcan is the can bus defined by MX_CAN_Init();

CAN_FilterConfTypeDef sFilterConfig;
sFilterConfig.FilterMode = CAN_FILTERMODE_IDLIST;
sFilterConfig.FilterScale = CAN_FILTERSCALE_16BIT;
sFilterConfig.FilterFIFOAssignment = CAN_FIFO0;
sFilterConfig.FilterActivation = ENABLE;

sFilterConfig.FilterNumber = 0;
sFilterConfig.FilterIdHigh = CANID_1;
sFilterConfig.FilterIdLow = CANID_1 << 5;
HAL_CAN_ConfigFilter(&hcan, &sFilterConfig);

sFilterConfig.FilterNumber = 1;
sFilterConfig.FilterIdHigh = CANID_2;
sFilterConfig.FilterIdLow = CANID_2 << 5;
HAL_CAN_ConfigFilter(&hcan, &sFilterConfig);

sFilterConfig.FilterNumber = 2;
sFilterConfig.FilterIdHigh = CANID_3;
sFilterConfig.FilterIdLow = CANID_3 << 5;
HAL_CAN_ConfigFilter(&hcan, &sFilterConfig);
~~~

This should be ran as often as possible as there seems to be a timeout/bug!

~~~ C
HAL_CAN_Receive_IT(&hcan, CAN_FIFO0);
~~~

### Recieve ISR
This is to be placed in main.c, but outside the main function. The ISR is called upon reception of the CAN message. It is weakly defined elsewhere in the HAL code.

~~~ C
void HAL_CAN_RxCpltCallback(CAN_HandleTypeDef *candle) {
  HAL_CAN_Receive_IT(candle, CAN_FIFO0);

  volatile uint8_t data[8];
  volatile uint8_t dlc;

  dlc = sRxMessage.DLC;
  data[0] = sRxMessage.Data[0];
  data[1] = sRxMessage.Data[1];
  data[2] = sRxMessage.Data[2];
  data[3] = sRxMessage.Data[3];
  data[4] = sRxMessage.Data[4];
  data[5] = sRxMessage.Data[5];
  data[6] = sRxMessage.Data[6];
  data[7] = sRxMessage.Data[7];

  // Code to be ran using the newly gathered data!
}
~~~
