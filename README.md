# STM32F0_Tutorial

Downaloding the System Workbench for STM32 installer
1. Go to http://www.openstm32.org/HomePage and sign-up/sign-in
2. Click on “System Workbench for STM32 - Bare Metal Edition”
3. Click on "Installing System Workbench for STM32 with installer"
4. Click on "Downloading the System Workbench for STM32 installer"
5. Select the most appropriate download for your OS. (Click on "install_sw4stm32_win_64bits-v2.6.exe" for Windows). 

Installing System Workbench for STM32 using the installer
1. Once it is downloaded, launch the System Workbench for STM32 installer.
2. Wait until the installer window is displayed.
3. The first page describes the product features. Click on “Next”.
4. Read the license agreements (if you dare) and accept them to continue the installation. Click on “Next” when you're done accepting them.
5. Choose the installation path (Default path: C:\Ac6\SystemWorkbench). Avoid using a path with spaces. Click on “Next”.
6. A warning message may be displayed :
    - If the directory does not exist, it proposes to create the installation directory. Click "OK".
    - If the directory exists, the installer will suppress the installation folder. Installing on top of older folders might cause several issues, so it's best to create a new one.
7. The next page shows the list of packs that will be installed. Click on “Next”.
8. On the next page, select if you want to create Start menu and desktop shortcuts.Click on “Next”.
9. The following page show the installation settings. Click on “Next” to proceed with the installation.
10. Wait until the installation is done and click on “Quit” when the “Finish” message is displayed on the installation progress.

Installing GNU MCU Eclipse
1. Launch System Workbench for STM32
2. Got to: "Help" > "Eclipse Marketplace...". Enter "GNU" into the Find bar, and then click "GO"
3. Install GNU MCU Eclipse, and wait for the installation to finish.
4. The program will restart once the installation is complete.

Updating System Workbench fo STM32
1. Launch System Workbench for STM32
2. Got to: "Help" > "Check for Updates". Wait for the program to finish checking for updates.
3. If there are no updates, then you're done. Otherwise, proceed to the next step.
4. The "Available Updates" page will appear. Select all the updates, then click "Next".
5. The "Update Details" page will appear. Click "Next".
6. Read the license agreement (if you dare) and accept it. Click "Finish", and wait for the update to complete. Click "OK" if a warning appears
7. The program will restart once the installation is complete.

Downloading the STM32CubeMX installer
1. Go to https://www.st.com/en/development-tools/stm32cubemx.html, scroll to the bottom and click on "Get Software".
2. Accept the "License Aggreement"
3. Click on "Login/Register" or enter your first name, last name and email address.
4. Accept the "Sales Terms & Conditions". Click on "Download".
5. Check your email. You should receive a link that should allow you to download the STM32CubeMX installer.

Installing STM32CubeMX
1. Launch the STM32CubeMX installer
2. Wait until the installer window is displayed.
3. When the first page appears, just click "Next".
4. Accept the license agreement, then click "Next".
5. Confirm the installation path, then click "Next".
6. A warning message may be displayed :
    - If the directory does not exist, it proposes to create the installation directory. Click "OK".
    - If the directory exists, the installer will suppress the installation folder. Installing on top of older folders might cause several issues, so it's best to create a new one.
7. On the next page, select if you want to create Start menu and desktop shortcuts.Click on “Next”.
8. Wait until the installation is done and click on “Quit” when the “Finish” message is displayed on the installation progress. 

Updating STM32CubeMX
1. Launch System STM32CubeMX
2. Got to: "Help" > "Manage embedded software packages". Select the latest version of STM32F0. Click "Install Now"
3. Wait for the installation to finish. The program will restart upon completion.
4. Got to: "Help" > "Check for Updates". Click on "Refresh".
3. If there are no updates, then you're done. Otherwise, proceed to the next step.
4. Select all the available updates, then click "Install Now".
5. The "Update Details" page will appear. Click "Next".
6. Accept the license agreement. Click "Next" (or "Finish", I don't really remember)
7. Wait for the update to complete. Click "OK" if a warning appears.
8. The program will restart once the installation is complete.